﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;


namespace Async_TCP_Proxy
{

    //The state object that holds information when in between
    //flow control from client-proxy to proxy-server. 
    //Constructor should take an argument, the buffer size, that 
    //specifies the size of the buffer. By default, buffer size will
    //be 256.
    public class Intermediary_Proxy_Buffer
    {

        //Our proxy client socket.
        public Socket proxySock_Client;

        //Buffer size for holding client data from the server.
        public static int bufferc_Size = 256;

        //Buffer for holding byte data from client.
        public byte[] client_Data_Buffer;


        //Our proxy server socket.
        public Socket proxySock_Serv;

        //Socket of the actual client.
        public Socket origin_Client;

        //Buffer size for server.
        public static int buffers_size = 1024;

        //Buffer for holding byte data from origin server.
        public byte[] server_Data_Buffer;

        //Data recieved as a string.
        public StringBuilder data_String;



        //Constructor.
        public Intermediary_Proxy_Buffer()
        {
            client_Data_Buffer = new byte[bufferc_Size];

            server_Data_Buffer = new byte[buffers_size];

            data_String = new StringBuilder();

        }


    }

}
