﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Threading;
using System.Net;

namespace Async_TCP_Proxy
{

    //Proxy establishing connection to remote device server.
    public class Proxy
    {
        /* ------------ CONSTANTS ------------ */

        //This is for myself cause im used to the socket class in C.
        private const AddressFamily AF_INET = AddressFamily.InterNetwork;
        private const SocketType SOCK_STREAM = SocketType.Stream;
        private const ProtocolType TCP = ProtocolType.Tcp;

        private const int BACKLOG = 150;

        /* ------------ GENERAL VARS ------------ */

        public Proxy(IPEndPoint local)
        {
            proxy_server_listener = new Socket(AF_INET, SOCK_STREAM, TCP);
            //Bind socket to local
            proxy_server_listener.Bind(local);

            proxClientSock = new Socket(AF_INET, SOCK_STREAM, TCP); 
        }

        /******* CLIENT-TO-PROXY CONNECTION --------------- */
        //Proxy acts as the server to the client.

        /* ------------ CLIENT-PROXY VARS ------------ */

        //Thread signaler.
        public static ManualResetEvent AllFinished = new ManualResetEvent(false);

        //Notification tht we succesfully connected to a client.
        public Action<Intermediary_Proxy_Buffer> OnClientConnected { get; set; }

        //Notification that we recieved data from the client, in our IPB client buffer.
        public Action<String, int> OnClientDataRecieved { get; set; }

        //Notification that the client disconnected.
        public Action OnClientDisconnected { get; set; }

        //Bool that confirms we have succesfully connected to the client. 
        public bool ClientProxy
        {
            get;
            private set;
        } = false;

        //Our proxy server socket, that binds to our local EP
        public Socket proxy_server_listener;


        //The proxy acts as an intermediary server tp
        //the client. It will recieve client data, 
        //and forward it to the actual server. 

        public void BeginListening()
        {
            //The proxy is NOT redirecting any connection requests that might
            //get sent to the server. The proxy acts as a server so 
            //we will assume that the clients will attempt connection to us,
            //thinking we are the actual server.

            try
            {
                //We already binded our socket to our local ep, so begin listening.
                proxy_server_listener.Listen(BACKLOG);

                //Perpetually listen for connections. 
                while (true)
                {
                    AllFinished.Reset();
                   
                    Console.WriteLine("Listening for connections. Stand by...");

                    proxy_server_listener.BeginAccept(new AsyncCallback(ClientProxy_AcceptCB),
                        proxy_server_listener);

                    //ensure a connection is made before proceeding
                    AllFinished.WaitOne();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occured while listening for client connections. ");
                Console.WriteLine(e.ToString());
            }
            Console.WriteLine("\n Connection found. Press ENTER to continue...");
            Console.Read();
        }

        //Callback func for accepting connections.
        private void ClientProxy_AcceptCB(IAsyncResult res)
        {
            // Signal the main thread to continue.  
            AllFinished.Set();
 
            //Our proxyServer_Socket.
            Socket listener = (Socket)res.AsyncState;

            //Socket that handles communication to the remote host.
            //In this case, the client.
            //AKA: This is the client socket!
            Socket client_handler = listener.EndAccept(res);

            Intermediary_Proxy_Buffer state = new Intermediary_Proxy_Buffer();

            //Record our proxy server socket info into our IPB.
            state.proxySock_Serv = listener;

            //Record the OG client's socket into our IPB.
            state.origin_Client = client_handler;

            ClientProxy = true;
            //Notify that we connected to a client. 
            //we made a connection. Notify any parties.
            if (OnClientConnected != null)
            {
                OnClientConnected(state);
            }
            //Wait, while we connect to the server. 


            //Start client communication.
            client_handler.BeginReceive(state.server_Data_Buffer, 0,
                Intermediary_Proxy_Buffer.buffers_size, 0, 
                new AsyncCallback(ClientProxy_ReadCB), state);
        }

        private void ClientProxy_ReadCB(IAsyncResult res)
        {
            String content = String.Empty;

            //Retrieve the intermediary, and the client's socket.
            Intermediary_Proxy_Buffer IPB = (Intermediary_Proxy_Buffer)res;
            Socket origin_Client = IPB.origin_Client;

            //Begin reading
            int bytesRead = origin_Client.EndReceive(res);

            //Make sure we get all the data
            if (bytesRead > 0)
            {
                IPB.data_String.Append(Encoding.ASCII.GetString(
                IPB.server_Data_Buffer, 0, bytesRead));

                content = IPB.data_String.ToString();
                if (content.IndexOf("<EOF>") > -1)
                {
                    // All the data has been read from the   
                    // client. Display it on the console.  
                    Console.WriteLine("Read {0} bytes from socket. \n Data : {1}",
                        content.Length, content);

                    //Notifyy that we recieved all data from client,
                    //So we forward to server.

                    if (OnClientDataRecieved != null)
                        OnClientDataRecieved(content, content.Length);
                }
                else
                {
                    // Not all data received. Get more.  
                    origin_Client.BeginReceive(IPB.server_Data_Buffer, 0, Intermediary_Proxy_Buffer.bufferc_Size, 0,
                    new AsyncCallback(ClientProxy_ReadCB), IPB);
                }
            }
        }
        
        //We recieved data from the server, so we send back to the client.
        public void Send_To_Client(Socket originClient, String data)
        {
            // Convert the string data to byte data using ASCII encoding.  
            byte[] byteData = Encoding.ASCII.GetBytes(data);

            // Begin sending the data to the remote device.  
            originClient.BeginSend(byteData, 0, byteData.Length, 0,
                new AsyncCallback(ClientProxy_SendCB), originClient);
        }

        //Proxy send data to client. Data is from OG server.
        private static void ClientProxy_SendCB(IAsyncResult res)
        {
            try
            {
                // Retrieve the socket from the state object.  
                Socket handler = (Socket)res.AsyncState;

                // Complete sending the data to the remote device.  
                int bytesSent = handler.EndSend(res);
                Console.WriteLine("Sent {0} bytes to client.", bytesSent);

                handler.Shutdown(SocketShutdown.Both);
                handler.Close();

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }


        /******* PROXY-TO-SERVER CONNECTION --------------- */
        //Here, the proxy acts as client on the actual client's behalf, to the dest server.

        /* ------------ PROXY-TO-SERVER VARS ------------ */

        private static ManualResetEvent connectDone =
            new ManualResetEvent(false);
        private static ManualResetEvent sendDone =
            new ManualResetEvent(false);
        private static ManualResetEvent receiveDone =
            new ManualResetEvent(false);

        //Our proxy client socket
        public Socket proxClientSock;

        private String response = String.Empty;

        public Action<String, int, Intermediary_Proxy_Buffer> OnServerDataRecieved;

        public bool ProxyServer
        {
            get;
            private set;
        } = false;

        public void StartClient(IPEndPoint ep_Server, Intermediary_Proxy_Buffer ipb)
        {
            try
            {
                ipb.proxySock_Client = proxClientSock;

                //Connect to destination server.
                proxClientSock.BeginConnect(ep_Server,
                    new AsyncCallback(ProxyServer_ConnectCB), proxClientSock);
                connectDone.WaitOne();

                //Recieve info from server.
                this.ProxyServ_Receive(proxClientSock, ipb);
                receiveDone.WaitOne();

            }
            catch (Exception e)
            {
                Console.WriteLine("Error found starting the Proxy Client during the Proxy-Server connection.");
                Console.WriteLine(e.ToString());
            }

        }

        //Callback function to connect to server.
        private void ProxyServer_ConnectCB(IAsyncResult res)
        {
            Socket proxy_Client = (Socket)res.AsyncState;

            //Complete the connection.  
            proxy_Client.EndConnect(res);

            ProxyServer = true;

            Console.WriteLine("Socket connected to {0}",
                proxy_Client.RemoteEndPoint.ToString());

            // Signal that the connection has been made.  
            connectDone.Set();
        }

        //We call Send when we are notified that we have data that the proxy recieved from
        //the actual client. We forward that data to the server.
        public void Send_To_Server(Socket proxy_Client, String data)
        {
            //Convert the string data to byte data using ASCII encoding.  
            byte[] byteData = Encoding.ASCII.GetBytes(data);

            // Begin sending the data to the remote device.
            //AKA -> the origin server.
            proxy_Client.BeginSend(byteData, 0, byteData.Length, 0,
                new AsyncCallback(ProxyServer_SendCB), proxy_Client);
        }

        private void ProxyServer_SendCB(IAsyncResult res)
        {
            try
            {
                // Retrieve the socket from the state object.  
                Socket cliProx = (Socket)res.AsyncState;

                // Complete sending the data to the remote device.  
                int bytesSent = cliProx.EndSend(res);
                Console.WriteLine("Sent {0} bytes to server.", bytesSent);

                // Signal that all bytes have been sent.  
                sendDone.Set();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error found in [ ProxyServer_SendCB ] during the Proxy-Server connection.");
                Console.WriteLine(e.ToString());
            }
        }

        //Recieve data from actual server.
        //client socket = our proxy_client socket.
        public void ProxyServ_Receive(Socket proxy_client, Intermediary_Proxy_Buffer state)
        {
            try
            {
                // Begin receiving the data from the remote device. 
                //remote device = origin server.
                proxy_client.BeginReceive(state.server_Data_Buffer, 0,
                    Intermediary_Proxy_Buffer.buffers_size, 0,
                    new AsyncCallback(ProxyServer_ReceiveCB), state);
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occured in [ ProxyServ_Receive ]. ");
                Console.WriteLine(e.ToString());
            }
        }

        private void ProxyServer_ReceiveCB(IAsyncResult res)
        {
            try
            {
                //Retrieve IPB, and proxy client socket
                Intermediary_Proxy_Buffer ipb = (Intermediary_Proxy_Buffer)res.AsyncState;
                Socket cliProxy = ipb.proxySock_Client;

                // Read data from the remote device (Origin server)
                int bytesRead = cliProxy.EndReceive(res);

                if (bytesRead > 0)
                {
                    // There might be more data, so store the data received so far.  
                    ipb.data_String.Append(Encoding.ASCII.GetString(ipb.client_Data_Buffer, 0, bytesRead));

                    // Get the rest of the data, into our IPB client buffer. 
                    cliProxy.BeginReceive(ipb.client_Data_Buffer, 0, Intermediary_Proxy_Buffer.bufferc_Size, 0,
                        new AsyncCallback(ProxyServer_ReceiveCB), ipb);
                }
                else
                {
                    // All the data has arrived; put it in response.  
                    if (ipb.data_String.Length > 1)
                    {
                        response = ipb.data_String.ToString();
                        //Send it back to the OG client
                        if (OnServerDataRecieved != null){
                            OnServerDataRecieved(response, response.Length, ipb);
                        }
                    }
                    // Signal that all bytes have been received.  
                    receiveDone.Set();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
    }


}
