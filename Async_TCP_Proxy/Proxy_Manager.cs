﻿using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Async_TCP_Proxy
{
    class Proxy_Manager
    {
        public static Proxy getProxy;

        public static IPEndPoint server;

        public Proxy_Manager(Proxy prox, IPEndPoint destServer)
        {
            server = destServer; 
            getProxy = prox;

            prox.OnClientConnected = ConnectToServer;
            prox.OnClientDataRecieved = ForwardToServer;
            prox.OnClientDisconnected = Disconnect;
            prox.OnServerDataRecieved = ForwardToClient;

        }

        public void ForwardStart()
        {
            getProxy.BeginListening();
        }

        private static void ConnectToServer(Intermediary_Proxy_Buffer ipb)
        {
            if (getProxy.ClientProxy)
            {
                //We connected to the origin client, so connect to the server
                //on the client's behalf.
                getProxy.StartClient(server, ipb);
            }
        }

        private static void ForwardToServer(string content, int size)
        {
            if (size <= Intermediary_Proxy_Buffer.bufferc_Size && 
                getProxy.ClientProxy && getProxy.ProxyServer)
            {
                getProxy.Send_To_Server(getProxy.proxClientSock, content);
            }
        }

        private static void ForwardToClient(String data, int size, Intermediary_Proxy_Buffer ipb)
        {
            if (size <= Intermediary_Proxy_Buffer.buffers_size &&
                getProxy.ClientProxy && getProxy.ProxyServer)
            {
                getProxy.Send_To_Client(ipb.origin_Client, data);
            }
        }

        private static void Disconnect()
        {

        }

    }

}
