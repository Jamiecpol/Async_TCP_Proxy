﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Async_TCP_Proxy
{
    //Instance of the TCP session.
    class Program
    {
        /*Variables that are arguments to the main function. We need the port of 
          of our machine, to host the proxy server, and the remote host name and port 
          which is the actual origin server. */
        private static int localPort;
        private static string remoteHost;
        private static int remotePort;

        /*IP Endpoints we make from the main args.*/


        //Endpoint of our machine
        private static IPEndPoint Local_ep;

        //Proxy
        private static Proxy GetProxy;

        //ProxyManager that will handle the sending and recieving of data
        private static Proxy_Manager proxMan;

        public static event EventHandler StartProxy;

        static void Main(string[] args)
        {
            //We need 3 arguments
            if (args.Length < 3)
            {
                Console.WriteLine("Usage : TcpProxy <local port> <remote host> <remote port>");
                return;
            }

            localPort = Convert.ToInt32(args[0]);
            remoteHost = args[1];
            remotePort = Convert.ToInt32(args[2]);

            //Create the local endpoint - The Proxy ep.
            Local_ep = new IPEndPoint(IPAddress.Any, localPort);

            //RemoteHost and remotePort will make the endpoint of the origin server.
            IPEndPoint Server_ep = new IPEndPoint(IPAddress.Parse(remoteHost), remotePort);

            //Create proxy
            GetProxy = new Proxy(Local_ep);

            //Create Proxy_Manager
            proxMan = new Proxy_Manager(GetProxy, Server_ep);

            Console.WriteLine("Proxy established. Initiating program.");

            proxMan.ForwardStart();
        }


    }
}
